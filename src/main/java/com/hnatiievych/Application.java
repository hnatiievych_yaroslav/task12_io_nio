package com.hnatiievych;

import com.hnatiievych.view.MyView;

public class Application {
    public static void main(String[] args) {
        MyView myView = new MyView();
        myView.show();
    }
}
