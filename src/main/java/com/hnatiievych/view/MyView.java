package com.hnatiievych.view;

import com.hnatiievych.Application;
import com.hnatiievych.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private ControllerImp controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(Application.class);

    public MyView() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Test serialization");
        menu.put("2", "  2 - Compare usual and buffered reader");
        menu.put("3", "  3 - ");
        menu.put("4", "  4 - ");
        menu.put("5", "  5 - ");
        menu.put("6", "  6 - ");
        menu.put("Q", "  Q - ");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        logger.info("Test serializable");
        controller.testSerialize();
        controller.testDeserialize();

    }

    private void pressButton2() {
        logger.info("Test reading and writting.Usual and Buffered");
        controller.compateReading();
    }

    private void pressButton3() {
        logger.info("");

    }

    private void pressButton4() {
        logger.info("");

    }

    private void pressButton5() {
        logger.info("");
    }

    private void pressButton6() {
        logger.info("");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
