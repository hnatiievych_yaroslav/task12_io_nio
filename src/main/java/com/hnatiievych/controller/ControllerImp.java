package com.hnatiievych.controller;

import com.hnatiievych.model.Droid;
import com.hnatiievych.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ControllerImp {
    private static Logger logger = LogManager.getLogger(ControllerImp.class);

    public void testSerialize() {
        List<Droid> droids = new ArrayList<>();
        droids.add(new Droid("first", 12, "droid"));
        droids.add(new Droid("second", 13, "droid"));
        droids.add(new Droid("third", 14, "droid"));
        droids.add(new Droid("fourth", 15, "droid"));
        Ship ship = new Ship(droids);
        try (ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream("ship.dat"))) {
            ous.writeObject(ship);
            System.out.println(ship.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void testDeserialize() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("ship.dat"))) {
            Ship ship2 = (Ship) ois.readObject();
            System.out.println(ship2.toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void compateReading() {
       logger.info(this.testUsualReader());
        logger.info(this.testBufferedReader());

    }

    private String testUsualReader() {
        int count = 0;
        long startTime = new Date().getTime();
        logger.info(startTime);
        try (InputStream is = new FileInputStream("Test.pdf")) {

            int data = is.read();
            count = 1;
            while (data != -1) {
                data = is.read();
                count++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = new Date().getTime();
        logger.info(finishTime);
        long time = finishTime-startTime;
        logger.info("Time : " +time);
        return "Size: " + count;
    }


    private String testBufferedReader() {
        int count = 0;
        long startTime = new Date().getTime();
        logger.info(startTime);
        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("Test.pdf")))) {

            while (true) {
                int i = dis.read();
                count++;

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = new Date().getTime();
        logger.info(finishTime);
        long time = finishTime-startTime;
        logger.info("Time: "+ time);
        return " Size: " + count;
    }
}
